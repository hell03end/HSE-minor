# -*- coding: utf-8 -*-
import scrapy
import urllib


class PricelistSpider(scrapy.Spider):
    name = "pricelist"
    allowed_domains = ["europegym.ru"]
    start_urls = ["https://www.europegym.ru/centers/prices/1.html"]

    def parse(self, response):
        SIDEBAR_LINK_SELECTOR = ".side_info:last-child a"
        LINK_SELECTOR = "::attr(href)"
        BASE_URL = "https://www.europegym.ru"
        for link in response.css(SIDEBAR_LINK_SELECTOR):
            yield scrapy.Request(
                url=urllib.parse.urljoin(
                    BASE_URL, 
                    link.css(LINK_SELECTOR).extract_first()
                ),
                callback=self.parse_center
            )

    @staticmethod
    def cell_text(td, lower=True):
        DIV_SELECTOR = "div"
        TEXT_SELECTOR = "::text"
        if len(td.css(DIV_SELECTOR)):
            TEXT_SELECTOR = "".join((DIV_SELECTOR, TEXT_SELECTOR))
        if lower:
            return td.css(TEXT_SELECTOR).extract_first().strip().lower()
        return td.css(TEXT_SELECTOR).extract_first().strip()

    def parse_center(self, response):
        PERSONAL_TRAININGS_SELECTOR = (
            # select certain header
            "//h2[contains(text(), \"Персональные тренировки\")]"
            "/following-sibling"  # all following elements
            "::table[1]"  # first table from all tables (count from 1)
        )
        PERSONAL_TD_SELECTOR = "".join((
            PERSONAL_TRAININGS_SELECTOR, 
            "/tr[position() > 1]"  # all table rows except first (descriptions)
        ))
        PERSONAL_TH_SELECTOR = "".join((
            PERSONAL_TRAININGS_SELECTOR,
            "/tr[1]/th"  # select table headers
        ))
        TABLE_DATA_SELECTOR = "td"
        H1_TEXT_SELECTOR = "h1::text"
        yield {
            'title': response.css(H1_TEXT_SELECTOR).extract_first().strip(),
            'headers': [PricelistSpider.cell_text(th)
                        for th in response.xpath(PERSONAL_TH_SELECTOR)],
            'table': [
                [PricelistSpider.cell_text(td) 
                 for td in row.xpath(TABLE_DATA_SELECTOR)]
                for row in response.xpath(PERSONAL_TD_SELECTOR)
            ]
        }
