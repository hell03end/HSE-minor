How to create scrapy project
-----------------------------
```bash
scrapy startproject <project-name>
cd <project-name>
scrapy genspider <filename> <entry-point>
# file will appear in <project-name>/spiders/<filename>.py
```

How to proceed Unicode
-----------------------
```bash
# add to settings.py
FEED_EXPORT_INDENT = 2
FEED_EXPORT_ENCODING = "utf-8"
```

How to run project
-------------------
```bash
scrapy crawl pricelist
# write result to file:
scrapy crawl pricelist -o <filename>
```